export const districts = [
    {
        "distrito": "1",
        "comuna": "Arica"
    },
    {
        "distrito": "1",
        "comuna": "Camarones"
    },
    {
        "distrito": "1",
        "comuna": "General lagos"
    },
    {
        "distrito": "1",
        "comuna": "Putre"
    },
    {
        "distrito": "2",
        "comuna": "Alto hospicio"
    },
    {
        "distrito": "2",
        "comuna": "Iquique"
    },
    {
        "distrito": "2",
        "comuna": "CamiÃ±a"
    },
    {
        "distrito": "2",
        "comuna": "Colchane"
    },
    {
        "distrito": "2",
        "comuna": "Huara"
    },
    {
        "distrito": "2",
        "comuna": "Pica"
    },
    {
        "distrito": "2",
        "comuna": "Pozo almonte"
    },
    {
        "distrito": "3",
        "comuna": "Antofagasta"
    },
    {
        "distrito": "3",
        "comuna": "Mejillones"
    },
    {
        "distrito": "3",
        "comuna": "Sierra gorda"
    },
    {
        "distrito": "3",
        "comuna": "Taltal"
    },
    {
        "distrito": "3",
        "comuna": "Calama"
    },
    {
        "distrito": "3",
        "comuna": "Ollague"
    },
    {
        "distrito": "3",
        "comuna": "San pedro de atacama"
    },
    {
        "distrito": "3",
        "comuna": "Maria elena"
    },
    {
        "distrito": "3",
        "comuna": "Tocopilla"
    },
    {
        "distrito": "4",
        "comuna": "ChaÃ±aral"
    },
    {
        "distrito": "4",
        "comuna": "Diego de almagro"
    },
    {
        "distrito": "4",
        "comuna": "Caldera"
    },
    {
        "distrito": "4",
        "comuna": "Copiapo"
    },
    {
        "distrito": "4",
        "comuna": "Tierra amarilla"
    },
    {
        "distrito": "4",
        "comuna": "Alto del carmen"
    },
    {
        "distrito": "4",
        "comuna": "Freirina"
    },
    {
        "distrito": "4",
        "comuna": "Huasco"
    },
    {
        "distrito": "4",
        "comuna": "Vallenar"
    },
    {
        "distrito": "5",
        "comuna": "Canela"
    },
    {
        "distrito": "5",
        "comuna": "Illapel"
    },
    {
        "distrito": "5",
        "comuna": "Los vilos"
    },
    {
        "distrito": "5",
        "comuna": "Salamanca"
    },
    {
        "distrito": "5",
        "comuna": "Andacollo"
    },
    {
        "distrito": "5",
        "comuna": "Coquimbo"
    },
    {
        "distrito": "5",
        "comuna": "La higuera"
    },
    {
        "distrito": "5",
        "comuna": "La serena"
    },
    {
        "distrito": "5",
        "comuna": "Paiguano"
    },
    {
        "distrito": "5",
        "comuna": "VicuÃ±a"
    },
    {
        "distrito": "5",
        "comuna": "Combarbala"
    },
    {
        "distrito": "5",
        "comuna": "Monte patria"
    },
    {
        "distrito": "5",
        "comuna": "Ovalle"
    },
    {
        "distrito": "5",
        "comuna": "Punitaqui"
    },
    {
        "distrito": "5",
        "comuna": "Rio hurtado"
    },
    {
        "distrito": "6",
        "comuna": "Calle larga"
    },
    {
        "distrito": "6",
        "comuna": "Los andes"
    },
    {
        "distrito": "6",
        "comuna": "Rinconada"
    },
    {
        "distrito": "6",
        "comuna": "San esteban"
    },
    {
        "distrito": "6",
        "comuna": "Limache"
    },
    {
        "distrito": "6",
        "comuna": "Olmue"
    },
    {
        "distrito": "6",
        "comuna": "Quilpue"
    },
    {
        "distrito": "6",
        "comuna": "Villa alemana"
    },
    {
        "distrito": "6",
        "comuna": "Cabildo"
    },
    {
        "distrito": "6",
        "comuna": "La ligua"
    },
    {
        "distrito": "6",
        "comuna": "Papudo"
    },
    {
        "distrito": "6",
        "comuna": "Petorca"
    },
    {
        "distrito": "6",
        "comuna": "Zapallar"
    },
    {
        "distrito": "6",
        "comuna": "Calera"
    },
    {
        "distrito": "6",
        "comuna": "Hijuelas"
    },
    {
        "distrito": "6",
        "comuna": "La cruz"
    },
    {
        "distrito": "6",
        "comuna": "Nogales"
    },
    {
        "distrito": "6",
        "comuna": "Quillota"
    },
    {
        "distrito": "6",
        "comuna": "Catemu"
    },
    {
        "distrito": "6",
        "comuna": "Llaillay"
    },
    {
        "distrito": "6",
        "comuna": "Panquehue"
    },
    {
        "distrito": "6",
        "comuna": "Putaendo"
    },
    {
        "distrito": "6",
        "comuna": "San felipe"
    },
    {
        "distrito": "6",
        "comuna": "Santa maria"
    },
    {
        "distrito": "6",
        "comuna": "Puchuncavi"
    },
    {
        "distrito": "6",
        "comuna": "Quintero"
    },
    {
        "distrito": "7",
        "comuna": "Isla de pascua"
    },
    {
        "distrito": "7",
        "comuna": "Algarrobo"
    },
    {
        "distrito": "7",
        "comuna": "Cartagena"
    },
    {
        "distrito": "7",
        "comuna": "El quisco"
    },
    {
        "distrito": "7",
        "comuna": "El tabo"
    },
    {
        "distrito": "7",
        "comuna": "San antonio"
    },
    {
        "distrito": "7",
        "comuna": "Santo domingo"
    },
    {
        "distrito": "7",
        "comuna": "Concon"
    },
    {
        "distrito": "7",
        "comuna": "ViÃ±a del mar"
    },
    {
        "distrito": "7",
        "comuna": "Casablanca"
    },
    {
        "distrito": "7",
        "comuna": "Juan fernandez"
    },
    {
        "distrito": "7",
        "comuna": "Valparaiso"
    },
    {
        "distrito": "8",
        "comuna": "Colina"
    },
    {
        "distrito": "8",
        "comuna": "Lampa"
    },
    {
        "distrito": "8",
        "comuna": "Tiltil"
    },
    {
        "distrito": "8",
        "comuna": "Pudahuel"
    },
    {
        "distrito": "8",
        "comuna": "Quilicura"
    },
    {
        "distrito": "8",
        "comuna": "Cerrillos"
    },
    {
        "distrito": "8",
        "comuna": "Estacion central"
    },
    {
        "distrito": "8",
        "comuna": "Maipu"
    },
    {
        "distrito": "9",
        "comuna": "Conchali"
    },
    {
        "distrito": "9",
        "comuna": "Huechuraba"
    },
    {
        "distrito": "9",
        "comuna": "Renca"
    },
    {
        "distrito": "9",
        "comuna": "Cerro navia"
    },
    {
        "distrito": "9",
        "comuna": "Independencia"
    },
    {
        "distrito": "9",
        "comuna": "Lo prado"
    },
    {
        "distrito": "9",
        "comuna": "Quinta normal"
    },
    {
        "distrito": "9",
        "comuna": "Recoleta"
    },
    {
        "distrito": "10",
        "comuna": "Santiago"
    },
    {
        "distrito": "10",
        "comuna": "ÃuÃ±oa"
    },
    {
        "distrito": "10",
        "comuna": "Providencia"
    },
    {
        "distrito": "10",
        "comuna": "La granja"
    },
    {
        "distrito": "10",
        "comuna": "Macul"
    },
    {
        "distrito": "10",
        "comuna": "San joaquin"
    },
    {
        "distrito": "11",
        "comuna": "La reina"
    },
    {
        "distrito": "11",
        "comuna": "Las condes"
    },
    {
        "distrito": "11",
        "comuna": "Lo barnechea"
    },
    {
        "distrito": "11",
        "comuna": "Vitacura"
    },
    {
        "distrito": "11",
        "comuna": "PeÃ±alolen"
    },
    {
        "distrito": "12",
        "comuna": "Pirque"
    },
    {
        "distrito": "12",
        "comuna": "Puente alto"
    },
    {
        "distrito": "12",
        "comuna": "San jose de maipo"
    },
    {
        "distrito": "12",
        "comuna": "La florida"
    },
    {
        "distrito": "12",
        "comuna": "La pintana"
    },
    {
        "distrito": "13",
        "comuna": "El bosque"
    },
    {
        "distrito": "13",
        "comuna": "La cisterna"
    },
    {
        "distrito": "13",
        "comuna": "Lo espejo"
    },
    {
        "distrito": "13",
        "comuna": "Pedro aguirre cerda"
    },
    {
        "distrito": "13",
        "comuna": "San miguel"
    },
    {
        "distrito": "13",
        "comuna": "San ramon"
    },
    {
        "distrito": "14",
        "comuna": "Buin"
    },
    {
        "distrito": "14",
        "comuna": "Calera de tango"
    },
    {
        "distrito": "14",
        "comuna": "Paine"
    },
    {
        "distrito": "14",
        "comuna": "San bernardo"
    },
    {
        "distrito": "14",
        "comuna": "Alhue"
    },
    {
        "distrito": "14",
        "comuna": "Curacavi"
    },
    {
        "distrito": "14",
        "comuna": "Maria pinto"
    },
    {
        "distrito": "14",
        "comuna": "Melipilla"
    },
    {
        "distrito": "14",
        "comuna": "San pedro"
    },
    {
        "distrito": "14",
        "comuna": "El monte"
    },
    {
        "distrito": "14",
        "comuna": "Isla de maipo"
    },
    {
        "distrito": "14",
        "comuna": "Padre hurtado"
    },
    {
        "distrito": "14",
        "comuna": "PeÃ±aflor"
    },
    {
        "distrito": "14",
        "comuna": "Talagante"
    },
    {
        "distrito": "15",
        "comuna": "Rancagua"
    },
    {
        "distrito": "15",
        "comuna": "Codegua"
    },
    {
        "distrito": "15",
        "comuna": "Coinco"
    },
    {
        "distrito": "15",
        "comuna": "Coltauco"
    },
    {
        "distrito": "15",
        "comuna": "DoÃ±ihue"
    },
    {
        "distrito": "15",
        "comuna": "Graneros"
    },
    {
        "distrito": "15",
        "comuna": "Machali"
    },
    {
        "distrito": "15",
        "comuna": "Malloa"
    },
    {
        "distrito": "15",
        "comuna": "Mostazal"
    },
    {
        "distrito": "15",
        "comuna": "Olivar"
    },
    {
        "distrito": "15",
        "comuna": "Quinta de tilcoco"
    },
    {
        "distrito": "15",
        "comuna": "Rengo"
    },
    {
        "distrito": "15",
        "comuna": "Requinoa"
    },
    {
        "distrito": "16",
        "comuna": "Las cabras"
    },
    {
        "distrito": "16",
        "comuna": "Peumo"
    },
    {
        "distrito": "16",
        "comuna": "Pichidegua"
    },
    {
        "distrito": "16",
        "comuna": "San vicente"
    },
    {
        "distrito": "16",
        "comuna": "La estrella"
    },
    {
        "distrito": "16",
        "comuna": "Litueche"
    },
    {
        "distrito": "16",
        "comuna": "Marchigue"
    },
    {
        "distrito": "16",
        "comuna": "Navidad"
    },
    {
        "distrito": "16",
        "comuna": "Paredones"
    },
    {
        "distrito": "16",
        "comuna": "Pichilemu"
    },
    {
        "distrito": "16",
        "comuna": "Chepica"
    },
    {
        "distrito": "16",
        "comuna": "Chimbarongo"
    },
    {
        "distrito": "16",
        "comuna": "Lolol"
    },
    {
        "distrito": "16",
        "comuna": "Nancagua"
    },
    {
        "distrito": "16",
        "comuna": "Palmilla"
    },
    {
        "distrito": "16",
        "comuna": "Peralillo"
    },
    {
        "distrito": "16",
        "comuna": "Placilla"
    },
    {
        "distrito": "16",
        "comuna": "Pumanque"
    },
    {
        "distrito": "16",
        "comuna": "San fernando"
    },
    {
        "distrito": "16",
        "comuna": "Santa cruz"
    },
    {
        "distrito": "17",
        "comuna": "Curico"
    },
    {
        "distrito": "17",
        "comuna": "HualaÃ±e"
    },
    {
        "distrito": "17",
        "comuna": "Licanten"
    },
    {
        "distrito": "17",
        "comuna": "Molina"
    },
    {
        "distrito": "17",
        "comuna": "Rauco"
    },
    {
        "distrito": "17",
        "comuna": "Romeral"
    },
    {
        "distrito": "17",
        "comuna": "Sagrada familia"
    },
    {
        "distrito": "17",
        "comuna": "Teno"
    },
    {
        "distrito": "17",
        "comuna": "Vichuquen"
    },
    {
        "distrito": "17",
        "comuna": "Constitucion"
    },
    {
        "distrito": "17",
        "comuna": "Curepto"
    },
    {
        "distrito": "17",
        "comuna": "Empedrado"
    },
    {
        "distrito": "17",
        "comuna": "Maule"
    },
    {
        "distrito": "17",
        "comuna": "Pelarco"
    },
    {
        "distrito": "17",
        "comuna": "Pencahue"
    },
    {
        "distrito": "17",
        "comuna": "Rio claro"
    },
    {
        "distrito": "17",
        "comuna": "San clemente"
    },
    {
        "distrito": "17",
        "comuna": "San rafael"
    },
    {
        "distrito": "17",
        "comuna": "Talca"
    },
    {
        "distrito": "18",
        "comuna": "Cauquenes"
    },
    {
        "distrito": "18",
        "comuna": "Chanco"
    },
    {
        "distrito": "18",
        "comuna": "Pelluhue"
    },
    {
        "distrito": "18",
        "comuna": "Colbun"
    },
    {
        "distrito": "18",
        "comuna": "Linares"
    },
    {
        "distrito": "18",
        "comuna": "Longavi"
    },
    {
        "distrito": "18",
        "comuna": "Parral"
    },
    {
        "distrito": "18",
        "comuna": "Retiro"
    },
    {
        "distrito": "18",
        "comuna": "San javier"
    },
    {
        "distrito": "18",
        "comuna": "Villa alegre"
    },
    {
        "distrito": "18",
        "comuna": "Yerbas buenas"
    },
    {
        "distrito": "19",
        "comuna": "Bulnes"
    },
    {
        "distrito": "19",
        "comuna": "Chillan"
    },
    {
        "distrito": "19",
        "comuna": "Chillan viejo"
    },
    {
        "distrito": "19",
        "comuna": "El carmen"
    },
    {
        "distrito": "19",
        "comuna": "Pemuco"
    },
    {
        "distrito": "19",
        "comuna": "Pinto"
    },
    {
        "distrito": "19",
        "comuna": "Quillon"
    },
    {
        "distrito": "19",
        "comuna": "San ignacio"
    },
    {
        "distrito": "19",
        "comuna": "Yungay"
    },
    {
        "distrito": "19",
        "comuna": "Cobquecura"
    },
    {
        "distrito": "19",
        "comuna": "Coelemu"
    },
    {
        "distrito": "19",
        "comuna": "Ninhue"
    },
    {
        "distrito": "19",
        "comuna": "Portezuelo"
    },
    {
        "distrito": "19",
        "comuna": "Quirihue"
    },
    {
        "distrito": "19",
        "comuna": "Ranquil"
    },
    {
        "distrito": "19",
        "comuna": "Trehuaco"
    },
    {
        "distrito": "19",
        "comuna": "Coihueco"
    },
    {
        "distrito": "19",
        "comuna": "Ãiquen"
    },
    {
        "distrito": "19",
        "comuna": "San carlos"
    },
    {
        "distrito": "19",
        "comuna": "San fabian"
    },
    {
        "distrito": "19",
        "comuna": "San nicolas"
    },
    {
        "distrito": "20",
        "comuna": "Hualpen"
    },
    {
        "distrito": "20",
        "comuna": "Penco"
    },
    {
        "distrito": "20",
        "comuna": "Talcahuano"
    },
    {
        "distrito": "20",
        "comuna": "Tome"
    },
    {
        "distrito": "20",
        "comuna": "Chiguayante"
    },
    {
        "distrito": "20",
        "comuna": "Concepcion"
    },
    {
        "distrito": "20",
        "comuna": "Florida"
    },
    {
        "distrito": "20",
        "comuna": "Coronel"
    },
    {
        "distrito": "20",
        "comuna": "Hualqui"
    },
    {
        "distrito": "20",
        "comuna": "San pedro de la paz"
    },
    {
        "distrito": "20",
        "comuna": "Santa juana"
    },
    {
        "distrito": "21",
        "comuna": "Arauco"
    },
    {
        "distrito": "21",
        "comuna": "CaÃ±ete"
    },
    {
        "distrito": "21",
        "comuna": "Contulmo"
    },
    {
        "distrito": "21",
        "comuna": "Curanilahue"
    },
    {
        "distrito": "21",
        "comuna": "Lebu"
    },
    {
        "distrito": "21",
        "comuna": "Los alamos"
    },
    {
        "distrito": "21",
        "comuna": "Tirua"
    },
    {
        "distrito": "21",
        "comuna": "Alto biobio"
    },
    {
        "distrito": "21",
        "comuna": "Antuco"
    },
    {
        "distrito": "21",
        "comuna": "Cabrero"
    },
    {
        "distrito": "21",
        "comuna": "Laja"
    },
    {
        "distrito": "21",
        "comuna": "Los angeles"
    },
    {
        "distrito": "21",
        "comuna": "Mulchen"
    },
    {
        "distrito": "21",
        "comuna": "Nacimiento"
    },
    {
        "distrito": "21",
        "comuna": "Negrete"
    },
    {
        "distrito": "21",
        "comuna": "Quilaco"
    },
    {
        "distrito": "21",
        "comuna": "Quilleco"
    },
    {
        "distrito": "21",
        "comuna": "San rosendo"
    },
    {
        "distrito": "21",
        "comuna": "Santa barbara"
    },
    {
        "distrito": "21",
        "comuna": "Tucapel"
    },
    {
        "distrito": "21",
        "comuna": "Yumbel"
    },
    {
        "distrito": "21",
        "comuna": "Lota"
    },
    {
        "distrito": "22",
        "comuna": "Galvarino"
    },
    {
        "distrito": "22",
        "comuna": "Lautaro"
    },
    {
        "distrito": "22",
        "comuna": "Melipeuco"
    },
    {
        "distrito": "22",
        "comuna": "Perquenco"
    },
    {
        "distrito": "22",
        "comuna": "Vilcun"
    },
    {
        "distrito": "22",
        "comuna": "Angol"
    },
    {
        "distrito": "22",
        "comuna": "Collipulli"
    },
    {
        "distrito": "22",
        "comuna": "Curacautin"
    },
    {
        "distrito": "22",
        "comuna": "Ercilla"
    },
    {
        "distrito": "22",
        "comuna": "Lonquimay"
    },
    {
        "distrito": "22",
        "comuna": "Los sauces"
    },
    {
        "distrito": "22",
        "comuna": "Lumaco"
    },
    {
        "distrito": "22",
        "comuna": "Puren"
    },
    {
        "distrito": "22",
        "comuna": "Renaico"
    },
    {
        "distrito": "22",
        "comuna": "Traiguen"
    },
    {
        "distrito": "22",
        "comuna": "Victoria"
    },
    {
        "distrito": "23",
        "comuna": "Padre las casas"
    },
    {
        "distrito": "23",
        "comuna": "Temuco"
    },
    {
        "distrito": "23",
        "comuna": "Carahue"
    },
    {
        "distrito": "23",
        "comuna": "Cholchol"
    },
    {
        "distrito": "23",
        "comuna": "Cunco"
    },
    {
        "distrito": "23",
        "comuna": "Curarrehue"
    },
    {
        "distrito": "23",
        "comuna": "Freire"
    },
    {
        "distrito": "23",
        "comuna": "Gorbea"
    },
    {
        "distrito": "23",
        "comuna": "Loncoche"
    },
    {
        "distrito": "23",
        "comuna": "Nueva imperial"
    },
    {
        "distrito": "23",
        "comuna": "Pitrufquen"
    },
    {
        "distrito": "23",
        "comuna": "Pucon"
    },
    {
        "distrito": "23",
        "comuna": "Saavedra"
    },
    {
        "distrito": "23",
        "comuna": "Teodoro schmidt"
    },
    {
        "distrito": "23",
        "comuna": "Tolten"
    },
    {
        "distrito": "23",
        "comuna": "Villarrica"
    },
    {
        "distrito": "24",
        "comuna": "Futrono"
    },
    {
        "distrito": "24",
        "comuna": "La union"
    },
    {
        "distrito": "24",
        "comuna": "Lago ranco"
    },
    {
        "distrito": "24",
        "comuna": "Rio bueno"
    },
    {
        "distrito": "24",
        "comuna": "Corral"
    },
    {
        "distrito": "24",
        "comuna": "Lanco"
    },
    {
        "distrito": "24",
        "comuna": "Los lagos"
    },
    {
        "distrito": "24",
        "comuna": "Mafil"
    },
    {
        "distrito": "24",
        "comuna": "Mariquina"
    },
    {
        "distrito": "24",
        "comuna": "Paillaco"
    },
    {
        "distrito": "24",
        "comuna": "Panguipulli"
    },
    {
        "distrito": "24",
        "comuna": "Valdivia"
    },
    {
        "distrito": "25",
        "comuna": "Fresia"
    },
    {
        "distrito": "25",
        "comuna": "Frutillar"
    },
    {
        "distrito": "25",
        "comuna": "Llanquihue"
    },
    {
        "distrito": "25",
        "comuna": "Los muermos"
    },
    {
        "distrito": "25",
        "comuna": "Puerto varas"
    },
    {
        "distrito": "25",
        "comuna": "Osorno"
    },
    {
        "distrito": "25",
        "comuna": "Puerto octay"
    },
    {
        "distrito": "25",
        "comuna": "Purranque"
    },
    {
        "distrito": "25",
        "comuna": "Puyehue"
    },
    {
        "distrito": "25",
        "comuna": "Rio negro"
    },
    {
        "distrito": "25",
        "comuna": "San juan de la costa"
    },
    {
        "distrito": "25",
        "comuna": "San pablo"
    },
    {
        "distrito": "26",
        "comuna": "Ancud"
    },
    {
        "distrito": "26",
        "comuna": "Castro"
    },
    {
        "distrito": "26",
        "comuna": "Chonchi"
    },
    {
        "distrito": "26",
        "comuna": "Curaco de velez"
    },
    {
        "distrito": "26",
        "comuna": "Dalcahue"
    },
    {
        "distrito": "26",
        "comuna": "Puqueldon"
    },
    {
        "distrito": "26",
        "comuna": "Queilen"
    },
    {
        "distrito": "26",
        "comuna": "Quellon"
    },
    {
        "distrito": "26",
        "comuna": "Quemchi"
    },
    {
        "distrito": "26",
        "comuna": "Quinchao"
    },
    {
        "distrito": "26",
        "comuna": "Calbuco"
    },
    {
        "distrito": "26",
        "comuna": "Cochamo"
    },
    {
        "distrito": "26",
        "comuna": "Maullin"
    },
    {
        "distrito": "26",
        "comuna": "Puerto montt"
    },
    {
        "distrito": "26",
        "comuna": "Chaiten"
    },
    {
        "distrito": "26",
        "comuna": "Futaleufu"
    },
    {
        "distrito": "26",
        "comuna": "Hualaihue"
    },
    {
        "distrito": "26",
        "comuna": "Palena"
    },
    {
        "distrito": "27",
        "comuna": "Aysen"
    },
    {
        "distrito": "27",
        "comuna": "Cisnes"
    },
    {
        "distrito": "27",
        "comuna": "Guaitecas"
    },
    {
        "distrito": "27",
        "comuna": "Cochrane"
    },
    {
        "distrito": "27",
        "comuna": "O'higgins"
    },
    {
        "distrito": "27",
        "comuna": "Tortel"
    },
    {
        "distrito": "27",
        "comuna": "Coyhaique"
    },
    {
        "distrito": "27",
        "comuna": "Lago verde"
    },
    {
        "distrito": "27",
        "comuna": "Chile chico"
    },
    {
        "distrito": "27",
        "comuna": "Rio ibaÃ±ez"
    },
    {
        "distrito": "28",
        "comuna": "Antartica"
    },
    {
        "distrito": "28",
        "comuna": "Cabo de hornos"
    },
    {
        "distrito": "28",
        "comuna": "Laguna blanca"
    },
    {
        "distrito": "28",
        "comuna": "Punta arenas"
    },
    {
        "distrito": "28",
        "comuna": "Rio verde"
    },
    {
        "distrito": "28",
        "comuna": "San gregorio"
    },
    {
        "distrito": "28",
        "comuna": "Porvenir"
    },
    {
        "distrito": "28",
        "comuna": "Primavera"
    },
    {
        "distrito": "28",
        "comuna": "Timaukel"
    },
    {
        "distrito": "28",
        "comuna": "Natales"
    },
    {
        "distrito": "28",
        "comuna": "Torres del paine"
    }
  ]
    
    // No modificar orden de los permisos, ya que podrían provocar errores de permisos en las consultas
    