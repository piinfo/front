import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import router from './router'
import {districts} from './constants/districts.js'

Vue.use(Vuex, axios, router);

export default new Vuex.Store({
    state: {
        selectedCommune: '',
        district: '0',
        districtsList: districts,
        top3Candidates: [],
        candidates: [],
        showCandidates: false
    },
    mutations: {
        getCommune(state, commune){
            state.district = state.districtsList.find(element => element.comuna.toUpperCase() === commune.toUpperCase()).distrito
            state.selectedCommune = commune
            state.showCandidates = false

        },
        postDataTop3Candidates(state, candidates)
        {
            state.top3Candidates = candidates
        },
        postDataCandidates(state, candidates)
        {
            state.candidates = candidates
            // eslint-disable-next-line no-console
            console.log(candidates)
            state.showCandidates = true
        },
        getPredictions(state)
        {
            axios.get('http://localhost:3000/forecasting/getForecasting')
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response.data)
                    state.predictions = response.data
                })
                .catch(error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                })
        },
    },
    actions:{
        getTop3Candidates(context, district)
        {
            // eslint-disable-next-line no-console
            console.log("asdasdas")
            axios.post('http://localhost:3000/search/top3-by-district', district)
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response.data)
                    context.commit('postDataTop3Candidates',response.data.hits)
                })
                .catch(error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                })
        },
        getCandidates(context,data)
        {
            // eslint-disable-next-line no-console
            console.log("asdasdas")
            axios.post('http://localhost:3000/search/string', data)
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response.data)
                    context.commit('postDataCandidates',response.data.result.hits)
                })
                .catch(error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                })
        },
    }
})
